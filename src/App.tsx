import React, { useState } from "react";
import "./App.css";
import logo from "./logo.svg";

function App() {

  const [input, setInput] = useState(null);

  const search = () => {
    console.log("searched");
  }

  return (
    <div className="container">
      <div className="row justify-content-center">
        <form className="glossary form-horizontal col-md-12 col-lg-8">
          <div className="col">
            <div style={{ textAlign: "center" }}>
              <img className="openscope-logo d-none d-lg-block" src={ logo } alt="OpenScope logo" />
            </div>
            <div>
              <div className="input-group">
                <input type="text" className="form-control" placeholder="OpenScope keresés…" />
                <div className="input-group-append" onClick={ search }>
                  <span className="input-group-text">
                    <img className="d-lg-none" src={ logo } />
                    <span className="glyphicon glyphicon-search d-none d-lg-block" style={{ paddingBottom: 3 + 'px' }}></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default App;
